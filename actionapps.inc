<?php
/**  AA Abstraction Layer
 *      developed for the actionkit project
 *
 *     All accesses to AA objects etc should be handled by this
 *
 *   (C)2006 Michael Moritz mimo/at/gn.apc.org
 *   version 0.2 2006/08
 *
 */
class AASlice
{
  ///public:
/*  function AASlice($slice_id,$slice_name) {
    $this->_slice_id = unpack_id128($slice_id);
    $this->_slice_name = $slice_name;
  }*/
  function AASlice($dbobject) {
    $this->_slice_id = unpack_id128($dbobject->f('id'));
    $this->_slice_name = $dbobject->f('name');
  }
  function getName() {
    return $this->_slice_name;
  }
  function addToArray(&$arr) {
//     return array($this->_slice_id=>$this->_slice_name);
    $arr[$this->_slice_id] = $this->_slice_name;
  }
  function getID() { return $this->_slice_id; }
  ///private:
  // unpacked id
  var $_slice_id;
  // slice name (string)
  var $_slice_name;
};
class AAView
{
  ///public:
  function AAView($dbobject) {
    $this->_id = $dbobject->f('id');
    $this->_name = $dbobject->f('name');
    $this->_type = $dbobject->f('type');
    $this->_slice_id = unpack_id128($dbobject->f('slice_id'));
  }
  function addToArray(&$arr) {
    $arr[$this->_id] = $this->_id." - ".$this->_name;
  }
  function getArray() {
    return array($this->_id => $this->_name.", ".$this->_type);
//     return array($this->_id."-".$this->_slice_id => $this->_name.", ".$this->_type);
  }
  function getID() { return $this->_id; }
  function getSliceID() { return $this->_slice_id; }
  function getName() { return $this->_name; }
  /**
    *
    * Find out whether this view is part of a slice or not
    *
  **/
  function isInSlice($slice_object) { return $this->_slice_id == $slice_object->getID(); }
  ///private:
  var $_id;
  var $_name;
  var $_type;
  var $_slice_id;
};
class AAField
{
  ///public:
  function AAField($name,$value) {
    $this->_name = $name;
    $this->_value = $value;
  }
  function getName() { return $this->_name; }
  function getValue() { return $this->_value; }
  ///private:
  var $_name;
  var $_value;
};
class AAItem
{
  function AAItem($item_id) {
//     drupal_set_message("AA Item $item_id");
//     $GLOBALS['debug'] = true;
    $content = GetItemContent($item_id);
//     huhl($content);
    if(!is_array($content) || count($content) === 0)
      return false;
    foreach($content[$item_id] as $field_id => $value) {
//       huhl($value);
//       $field_id = str_replace('.',"",$field_id);
      $this->_fields[] = new AAField($field_id,$value[0]['value']);
//       drupal_set_message("$field_id -> $value");
    }
  }
  function getFieldsArray() {
    $names = array();
    foreach($this->_fields as $aaitem) {
      $names[$aaitem->getName()] = $aaitem->getValue();
    }
    return $names;
  }
  var $_fields = array();
};
class AAInstall
{
  ///public:
  function AAInstall($path) {
    $this->_path = $path;
  }
  /**
   *
   * Return an array of AASlice objects
   *
   */
  function listSlices() {
    if(!empty($this->_slices))
      return $this->_slices;
    $this->_init();
    $slices = array();
    
    ///TODO is there an AA api function for this?
    $SQL = "SELECT `name`,`id` FROM slice ORDER BY `name`";
    $db = getDB();
    $db->query($SQL);
    while ($db->next_record()) {
      $slices[] = new AASlice($db);
    }
    freeDB($db);
    $this->_slices = $slices;
    return $slices;
  }
  /**
   *
   * Return an array of AAView objects
   * @param slice AASlice object
   *
   */
/*
  function listViews($slice_object) {
    $views = array();
    $this->_getViews();
    foreach($this->_views as $view_object) {
      if($view_object->isInSlice($slice_object))
        $views[] = $view_object;
    }
    return $views;
  */
  function listViews($slice_id) {
    $this->_init();
//     $views = array();
//     drupal_set_message($slice);
    ///TODO is there an AA api function for this?
    $SQL = "SELECT `id`,`name`,`type`,`slice_id` FROM view";
    $SQL .= ($slice != -1) ? " WHERE `slice_id`='".q_pack_id($slice_id)."'" : "";
    $SQL .= " ORDER BY `id`";
//     drupal_set_message($SQL);
    $db = getDB();
    $db->query($SQL);
    while ($db->next_record()) {
      $views[] = new AAView($db);
    }
    freeDB($db);
    return $views;
  }
  /**
   *
   * Return an ActionApps view - the HTML output
   * @param slice_id AASliceID (not used)
   * @param view_id AAViewID 
   * @param $params Parameters passed to the view (like vid=)
   * @return HTML for the view 
   */
  function getView($slice_id,$view_id,$params) {
    global $AA_INC_PATH, $AA_BASE_PATH, $AA_INSTAL_PATH,
      $spareDBs,$tracearr,$traceall,$allviews,$VIEW_SORT_DIRECTIONS,$MODULES,
      $QuoteArray,$UnQuoteArray,$debug;
    $debug = false;
    $this->_init();
    $this->_viewinit();
    /// convert the url params into param array (AA API)
    if($params && strlen($params))
    	$params = ParseViewParameters($params);
    
    /// add the view_id
    $params['vid'] = $view_id;
    
    $output = GetView($params);
    return $output;
  }  
  /**
   *
   * Return an ActionApps item object
   * @param item_id AA item ID
   * @return AAItem Object or false if not found
   */
  function getItem($item_id) {
    $this->_init();
    return new AAItem($item_id);
  }
  /**
   *
   * Store an ActionApps item object
   * @param item_id AA item ID
   * @param content_array content (key=>value)
   * @return item_id or false if failed
   */
  function updateItem($item_id,$content_array) {
    global $event, $itemvarset, $db, $debug;
    $debug = false;
    $debugsi = false;
    $this->_init();
    $item = new ItemContent($item_id);
//     huhl($content_array);
    foreach($content_array as $key=>$val) {
      $item->setValue($key,$val);
    }
//     huhl($item);
//     die;
    require_once $GLOBALS["AA_INC_PATH"]."event.class.php3";
    require_once $GLOBALS["AA_INC_PATH"]."itemfunc.php3";
    if ( !is_object($event) ) 
    	$event = new aaevent;
    return $item->storeItem( "update" );
  }
  /**
   *
   * Return an array of names
   *
   */
  function getArrayFromArray(&$arr) {
    $names = array();
    if(!is_array($arr))
      return $names;
    foreach($arr as $item) {
//       $names[] = $item->getArray();
      $item->addToArray($names);
    }
    return $names;
  }
  ///private:
  var $_slices;
  var $_views;
  var $_path;
  function _init() {
    static $s_initialised = false;
    if($s_initialised)
      return;
    global $AA_INC_PATH, $AA_BASE_PATH, $AA_INSTAL_PATH,
      $spareDBs,$tracearr,$traceall,$allviews,$VIEW_SORT_DIRECTIONS,$MODULES,
      $QuoteArray,$UnQuoteArray,$debug;
    require_once($this->_path);
//     // now mess around with the globals
    require_once $GLOBALS['AA_INC_PATH'].'easy_scroller.php3';
    require_once $GLOBALS['AA_INC_PATH'].'util.php3';
    require_once $GLOBALS["AA_INC_PATH"]."item.php3";
    require_once $GLOBALS["AA_INC_PATH"]."view.php3";
    require_once $GLOBALS["AA_INC_PATH"]."discussion.php3";
    require_once $GLOBALS["AA_INC_PATH"]."pagecache.php3";
    require_once $GLOBALS["AA_INC_PATH"]."searchlib.php3";
    require_once $GLOBALS["AA_INC_PATH"]."locsessi.php3";
    $s_initialised = true;
//     huhl($MODULES);
  }
  function _viewinit() {
/*    require_once($this->_path);
    // now mess around with the globals
    require_once $GLOBALS['AA_INC_PATH'].'easy_scroller.php3';
    require_once $GLOBALS['AA_INC_PATH'].'util.php3';
    require_once $GLOBALS["AA_INC_PATH"]."item.php3";
    require_once $GLOBALS["AA_INC_PATH"]."view.php3";
    require_once $GLOBALS["AA_INC_PATH"]."discussion.php3";
    require_once $GLOBALS["AA_INC_PATH"]."pagecache.php3";
    require_once $GLOBALS["AA_INC_PATH"]."searchlib.php3";
    require_once $GLOBALS["AA_INC_PATH"]."locsessi.php3";*/
  }
  
  function _getViews() {
    if(! empty($this->_views))
      return;
    $this->_init();
    $SQL = "SELECT `id`,`name`,`type`,`slice_id` FROM view";
    $db = getDB();
    $db->query($SQL);
    while ($db->next_record()) {
      $this->_views[] = new AAView($db);
    }
    freeDB($db);
  }
};
 
?>