
Readme
------
aaview displays ActionApps View output inside Drupal through a simple configuration frontend
aaitem displays ActionApps Items inside Drupal 

** The complete documentation can be found on http://actionkit.gn.apc.org **

Install
-------
aaview and aaitem depends on a local ActionApps installation and the ActiveForms Drupal module.
* ActionApps (tested with version 2.81): 
  - Installation Guide: http://www.actionapps.org/en/Installation_Guide
  - Download: http://sourceforge.net/projects/apc-aa/
* ActiveForms Drupal module
  - Download (not official drupal module): http://groups.drupal.org/node/317

Usage:
------
After enabling the aaview, aaitem and the activeforms modules, the ActionApps installation path needs to be set. Go to 

Administer | aaview | AA View Settings

Enter where you installed ActionApps (e.g. /var/www/htdocs/www.mysite.com/apc-aa)

Then go to Administer | blocks and configure the ActionApps View. First select the Slice, then the list of Views will update. Optionally, you can add view parameters to this (see http://www.actionapps.org/en/Url_Parameters#View_parameters)

Todos
-----
* Add support for slice based views (Index, Fulltext, Design)
* Find some method of (automatic) URL conversion
* Allow for more than one aaview block per site
* Add node type aaview

Hints for ActionApps Installation
---------------------------------
Many sites use a shared ActionApps installation. The configuration for these usually is to run the ActionApps installation folders as PHP Safe_Mode off. In a scenario like this it's necessary to run a Drupal that wants to use aaview also with Safe_Mode off.
For a more secure setup Drupal and ActionApps can be set up with the same user. In this case Safe_Mode only needs to be turned off for ActionApps (and even this might change with one of the next releases of ActionApps).


Details
-------
For the technically interested: aaview and aaitem use an ActionApps interface class (see actionapps.inc). The idea is to handle any calls to AA inside this class rather than putting them straight into the module. 


Author
------
(C)2006 Michael Moritz <mimo/at/gn.apc.org>
